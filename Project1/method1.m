function pi_final = method1(point_eval)

pi_eval = []; % record all the pi values
error = [];   % record all errors
cost = [];    % record all costs (running time)
count = 0;    % the number of points inside the circle
start_time = tic; % starting time
for p = 20:point_eval
    
    % generate random point
    px = rand(1);
    py = rand(1);
    
    if (px^2 + py^2 <= 1) % check if it is inside the circle
        count = count+1;
    end
    run_time = toc(start_time);
    pi_eval = [pi_eval, count/p*4];     % record current pi values
    error = [error, abs(pi-count/p*4)]; % record current error
    cost = [cost, run_time];            % record current cost
end
pi_final = pi_eval(end);

%% Plot PI values, errors, and costs along with the number of random points
figure, subplot(3,1,1),plot(pi_eval,'.b'), hold on;
plot(ones([1,point_eval])*pi, '.r'); 
axis([20 point_eval 0 inf]), ylim([1.8 3.5]),  title('The calculated PI.');
subplot(3,1,2),plot(error,'.b'), axis([20 point_eval 0 inf]), ylim([0 1.8]), title('The Errors.');
subplot(3,1,3),plot(cost,'.b'), axis([20 point_eval 0 inf]), title('Running time.'); xlabel('Numbers of random points (point\_eval)'), ylabel('Runing time (s)'),


%% Not used
function [res, err, run_time] = eval_pi(num_points)

count = 0;
start_time = tic;
for i = 1:num_points
    px = rand(1);
    py = rand(1);
    if(px^2 + py^2 <= 1)
        count = count+1;
    end 
end
res = count / num_points * 4;
run_time = toc(start_time);
err = abs(pi - res);
